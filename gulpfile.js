var gulp = require('gulp');
var connect = require('gulp-connect');
var Server = require('karma').Server;
var jshint = require('gulp-jshint');

gulp.task('connect', function () {
  connect.server({
    root: 'app',
    port: 8001
  });

  gulp.watch('app/scripts/*.js', ['jshint']);
});

gulp.task('default', ['connect']);


gulp.task('test', function(done){
	new Server({
		configFile: __dirname + '/karma.conf.js'
	}, done).start();
});

gulp.task('jshint', function() {
  return gulp.src('app/scripts/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});
  