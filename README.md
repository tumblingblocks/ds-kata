#ds-vend

### A vending machine code kata, following https://github.com/guyroyse/vending-machine-kata


To run in dev mode:

```npm install```

Run web server:

```gulp```

Will be available at http://localhost:8001/.

Start tests (in new terminal):

```gulp test```