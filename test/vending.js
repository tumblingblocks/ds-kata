/* vending machine */
describe('vending machine', function () {

	beforeEach(angular.mock.module('vend'));

  var $controller, scope, $timeout;

  beforeEach(inject(function(_$controller_, $rootScope, _$timeout_){
    
    $controller = _$controller_;
    $timeout = _$timeout_;
    scope = $rootScope.$new();
    scope.transactionCoins = [];
    scope.returnedCoins = [];
    scope.totalInserted = '$0.00';

    VendingMachineCtrl = $controller('VendingMachineCtrl', {
    	$scope: scope
    });

  }));

	var coin = {
		diameter: 21,
		weight: 5
	};

	var quarter = {
		weight: 5.6,
		diameter: 24
	};

	var dime = {
		weight: 2.26,
		diameter: 17
	};

	/* accepts coins */

	describe('accepts coins', function () {


	  it('should know the types of coins by diameter and weight', function () {
	  	var myCoin = scope.idCoin(coin);
	    expect(myCoin.value).toEqual(5);
	  });

	  it('should prompt the user to enter coins', function(){
	  	expect(scope.displayMessage).toEqual('INSERT COINS');
	  });

	  it('should allow for the insertion of coins', function(){
	  	expect(scope.transactionCoins).toEqual([]);
	  	scope.insertCoin(coin);
	  	expect(scope.transactionCoins[0].weight).toEqual(coin.weight);
	  });

	  it('should reject invalid coins', function(){
	  	var coin = {
	  		diameter: 26,
	  		weight: 8
	  	};	
	  	scope.insertCoin(coin);
	  	expect(scope.transactionCoins).toEqual([]);
	  	expect(scope.returnedCoins[0].weight).toEqual(coin.weight);
	  });

	  it('should keep a running total of entered coins', function(){
	  	scope.insertCoin(coin);
	  	scope.insertCoin(coin);
	  	scope.insertCoin(coin);

	  	expect(scope.transactionCoins.length).toEqual(3);

	  	expect(scope.insertedValue()).toEqual(15);
	  });

	  it('should show dollar amount inserted', function(){
	  	scope.insertCoin(coin);
	  	scope.insertCoin(coin);

	  	expect(scope.totalInserted).toEqual('$0.10');

	  });

	});


/* allows for product selection */

	describe('allows for product selection', function () {

		it('should know the cost and name of the products it vends', function () {
	    expect(scope.products.length).toEqual(3);
	    expect(scope.productInfo('cola').cost).toEqual(100);
	    expect(scope.productInfo('nuts').cost).toBeUndefined;
	  });

	  it('should check that enough money has been entered for the desired product', function(){

	  	scope.insertCoin(coin);
	  	scope.selectProduct('chips');
	  	expect(scope.displayMessage).toEqual('PRICE: $0.50');

	  	scope.insertCoin(quarter);
	  	scope.insertCoin(quarter);

	  	scope.selectProduct('chips');
	  	expect(scope.displayMessage).toEqual('THANK YOU');

	  });

	  it('should update inventory when an item is purchased', function(){
	  	scope.insertCoin(quarter);
	  	scope.insertCoin(quarter);
	  	scope.insertCoin(quarter);

	  	expect(scope.inventory.candy).toEqual(4);	
	  	scope.selectProduct('candy');
	  	expect(scope.inventory.candy).toEqual(3);	
	  });

	});

/* makes change */
	describe('making change', function () {

		it ('should return coins to the customer as change, giving the fewest possible coins', function(){

	  	scope.insertCoin(quarter);
	  	scope.insertCoin(quarter);
	  	scope.insertCoin(quarter);

	  	scope.selectProduct('candy');
	  	expect(scope.returnedCoins).toEqual([{name: 'dime', weight: 2.26, diameter: 17, value: 10}]);

		});
		
		it ('should organize the change for display', function(){
			// an actual vending machine doesn't need this because change is dumped in a pile/  This is for display.
			scope.insertCoin(quarter);
			scope.insertCoin(coin);

			scope.coinReturn();

			expect(scope.aggReturnedCoins).toEqual({penny: 0, nickel:1, dime: 0, quarter: 1, dollar: 0});

		});

	});

/* returns coins */
	describe('returning coins', function () {
		it('should allow all coins to be returned before selection', function(){
			scope.insertCoin(quarter);
			scope.insertCoin(coin);

			expect(scope.returnedCoins.length).toBe(0);
			scope.coinReturn();
			expect(scope.returnedCoins.length).toBe(2);
			expect(scope.returnedCoins).toEqual([{name: 'quarter', weight: 5.6, diameter: 24, value: 25}, {name: 'nickel', weight: 5, diameter: 21, value: 5}])
			expect(scope.displayMessage).toEqual('INSERT COINS');

		});

		it('should not return any coins, other than change, after selection', function(){
			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.insertCoin(quarter);

			expect(scope.returnedCoins.length).toBe(0);
			scope.selectProduct('candy');
			scope.coinReturn();
			expect(scope.returnedCoins).toEqual([{name: 'dime', weight: 2.26, diameter: 17, value: 10}]);
			expect(scope.displayMessage).toEqual('THANK YOU');

			$timeout.flush();
			expect(scope.displayMessage).toEqual('INSERT COINS');

		});
	});

/* announces sold out status */
	describe('announcing sold out status', function () {

		it('should not allow vending of sold out products', function(){
			scope.insertCoin(quarter);
	  	scope.insertCoin(quarter);
	  	scope.insertCoin(quarter);
	  	scope.insertCoin(quarter);

	  	scope.selectProduct('cola');
	  	expect(scope.displayMessage).toEqual('SOLD OUT');

		});

	});

/* exact change only, when can't make change */
	describe('tracking coins inside the machine', function () {

		it('should keep track of coins used for purchase and change', function(){
			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.insertCoin(quarter);

			scope.selectProduct('candy');
			expect(scope.bank.quarter).toEqual(3);
			expect(scope.bank.dime).toEqual(1);

		});

		it('should make change with smaller coins when larger coins are unavailable', function(){
			//for this test, remove dimes
			scope.clearCoin('dime');

			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.insertCoin(quarter);

			scope.selectProduct('candy');

			expect(scope.bank.nickel).toEqual(0);

		});

		
	//EXACT CHANGE
	// what needs change?
	// - at 50 cents, any extra would just be returned, same for 100
	// - only in the case of 65 cents would we need to worry about making change
	// possible ways to get to 65 while still being over
	// - 70 cents: 7 dimes or 2 quarters and 3 dimes.  need one nickel returned
	// - 75 cents: 3 quarters. need 10 cents returned.  2 nickels or 1 dime
	// need at least 1 dime and 1 nickel OR 2 nickels.  
		it('should announce exact change status, if not enough money to give back 10 cents', function(){

			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.selectProduct('candy');

			expect(scope.bank.dime).toEqual(1);
			expect(scope.displayMessage).toEqual('THANK YOU');
			$timeout.flush();

			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.selectProduct('candy');
			expect(scope.bank.dime).toEqual(0);
			$timeout.flush();

			expect(scope.bank.nickel).toEqual(2);
			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.selectProduct('candy');
			expect(scope.bank.nickel).toEqual(0);
			$timeout.flush();

			expect(scope.displayMessage).toEqual('EXACT CHANGE ONLY');

		});

		it('should announce exact change status, if not enough money to give back 5 cents', function(){

			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.insertCoin(dime);
			scope.insertCoin(dime);
			scope.selectProduct('candy');

			expect(scope.bank.nickel).toEqual(1);
			expect(scope.displayMessage).toEqual('THANK YOU');
			$timeout.flush();

			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.insertCoin(dime);
			scope.insertCoin(dime);
			scope.selectProduct('candy');
			expect(scope.bank.nickel).toEqual(0);
			$timeout.flush();

			expect(scope.displayMessage).toEqual('EXACT CHANGE ONLY');

		});

		it('should not allow purchase when exact change and not exact change provided', function(){
			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.insertCoin(dime);
			scope.insertCoin(dime);
			scope.selectProduct('candy');
			$timeout.flush();

			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.insertCoin(dime);
			scope.insertCoin(dime);
			scope.selectProduct('candy');
			$timeout.flush();

			expect(scope.displayMessage).toEqual('EXACT CHANGE ONLY');

			scope.insertCoin(quarter);
			scope.insertCoin(quarter);
			scope.insertCoin(dime);
			scope.insertCoin(dime);
			scope.selectProduct('candy');

			expect(scope.displayMessage).toEqual('EXACT CHANGE ONLY - SELECT ANOTHER');


		});

	});


});