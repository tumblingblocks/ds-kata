angular.module('vend', [])
  .controller('VendingMachineCtrl', ['$scope', '$timeout', function( $scope, $timeout ) {


  	/* set defaults */

  	// the vending machine knows coins
		// we could get these from somewhere else...
		var coins = [
			{
				name: 'penny',
				weight: 2.5,
				diameter: 19,
				value: 1
			},
			{
				name: 'nickel',
				weight: 5,
				diameter: 21,
				value: 5
			},
			{
				name: 'dime',
				weight: 2.26,
				diameter: 17,
				value: 10
			},
			{
				name: 'quarter',
				weight: 5.6,
				diameter: 24,
				value: 25
			},
			{
				name: 'dollar',
				weight: 8,
				diameter: 26,
				value: 100
			}
		];


		// the vending machine knows products
		$scope.products = [
			{
				name: 'cola',
				cost: 100
			},
			{
				name: 'chips',
				cost: 50
			},
			{
				name: 'candy',
				cost: 65
			}
		];

		// the vending maching has a starting inventory
		$scope.inventory = {
			cola: 0,
			chips: 5,
			candy: 4
		};

		// the vending machine starts with some money.  
		// the vending machine is aware of what types of coin it has - they were identified on entry.
		$scope.bank = {
			nickel: 2,
			dime: 2,
			quarter: 0
		};

		// set defaults
		$scope.displayMessage = 'INSERT COINS';
		$scope.transactionCoins = [];
		$scope.returnedCoins = [];
		$scope.totalInserted = '$0.00';
		$scope.aggReturnedCoins = {penny: 0, nickel:0, dime: 0, quarter: 0, dollar: 0};
		$scope.exactChangeOnly = false;


		//identify a coin
		$scope.idCoin = function(coinObj) {
			for (var i=0; i<coins.length; i++){
				if (coinObj.weight === coins[i].weight && coinObj.diameter === coins[i].diameter){
					return coins[i];
				}
			}
			return;
		};

		//identify a product
		$scope.productInfo = function(product){
			for (var i=0; i< $scope.products.length; i++){
				if(product === $scope.products[i].name){
					return $scope.products[i];
				}
			}
			return [];
		};

		//get display message
		$scope.getDisplayMessage = function(){
			$scope.displayMessage = 'INSERT COINS';
			checkExactChangeOnly();
			if ($scope.exactChangeOnly){
				$scope.displayMessage = 'EXACT CHANGE ONLY';
			}
			
			return $scope.displayMessage;
		};

		// value of all inserted coins
		$scope.insertedValue = function(){
			var value = 0;

			for (var i=0; i < $scope.transactionCoins.length; i++){
				value += $scope.transactionCoins[i].value;
			}
			return value;
		};

		// allow for the insertion of coins
		$scope.insertCoin = function(coinObj) { 
			var coin = $scope.idCoin(coinObj);

			//dollar coins and pennies are not valid.
			if (coin.name === 'dollar' || coin.name === 'penny'){
				$scope.returnedCoins.push(coin);
				aggregateReturnedCoins();
				return;
			}

			$scope.transactionCoins.push(coin);
			$scope.totalInserted = $scope.dollarFormat($scope.insertedValue());
			$scope.displayMessage = 'INSERTED: ' + $scope.totalInserted;
		};

		// put the right amount of change in the coin return
		var makeChange = function(inserted, cost){
			var changeAmount = inserted - cost;
			
			while (changeAmount > 0){ 

				//add to returned coins
				if (changeAmount >= 25 && $scope.bank.quarter > 0){ 
					$scope.bank.quarter = $scope.bank.quarter - 1;
					$scope.returnedCoins.push({name: 'quarter', weight: 5.6, diameter: 24, value: 25});
					changeAmount = changeAmount - 25;
				}
				if (changeAmount >= 10 && $scope.bank.dime > 0){
					$scope.bank.dime = $scope.bank.dime - 1;
					$scope.returnedCoins.push({name: 'dime', weight: 2.26, diameter: 17, value: 10});
					changeAmount = changeAmount - 10;
				}
				if (changeAmount >= 5 && $scope.bank.nickel > 0){ 
					$scope.bank.nickel = $scope.bank.nickel - 1;
					$scope.returnedCoins.push({name: 'nickel', weight: 5, diameter: 21, value: 5});
					changeAmount = changeAmount - 5;
				}

			}

			aggregateReturnedCoins();

		};

		var checkExactChangeOnly = function(){

			//check for exact change
			if ( ($scope.bank.nickel === 0) || ($scope.bank.nickel < 2 && $scope.bank.dime === 0 ) ){
				$scope.exactChangeOnly = true;
			}

		};


		// select and vend product
		$scope.selectProduct = function(product) {
			product = $scope.productInfo(product);
			var cost = product.cost;
			var centsEntered = $scope.insertedValue();

			if (cost > centsEntered){
				$scope.displayMessage = 'PRICE: ' + $scope.dollarFormat(cost);
				return;
			}

			// enforce exact change
			if (centsEntered > cost && $scope.exactChangeOnly){
				$scope.displayMessage = 'EXACT CHANGE ONLY - SELECT ANOTHER';
				return;
			}

			if (cost <= centsEntered){

				if ($scope.inventory[product.name] === 0) { 
					$scope.displayMessage = 'SOLD OUT';
					return;
				}

				//deduct from inventory
				$scope.inventory[product.name] = $scope.inventory[product.name] - 1;


				//add money to our bank, and clear transaction
				for (var i=0; i < $scope.transactionCoins.length; i++){
					$scope.bank[$scope.transactionCoins[i].name] = $scope.bank[$scope.transactionCoins[i].name] + 1;
				}

				//make change 
				makeChange(centsEntered, cost);

				//clear transaction
				$scope.transactionCoins = [];

			}

			$scope.displayMessage = 'THANK YOU';
			checkExactChangeOnly();

			$timeout(function(){
				$scope.displayMessage = $scope.getDisplayMessage();
			}, 5000);
		};

		var aggregateReturnedCoins = function(){
			$scope.aggReturnedCoins = {penny: 0, nickel:0, dime: 0, quarter: 0, dollar: 0};
			for (var i=0; i<$scope.returnedCoins.length; i++){
				$scope.aggReturnedCoins[$scope.returnedCoins[i].name] = $scope.aggReturnedCoins[$scope.returnedCoins[i].name] +1;
			}
		};

		// allow user to return coins at will
		$scope.coinReturn = function(){
			if ($scope.transactionCoins.length){
				for(var i=0; i < $scope.transactionCoins.length; i++){
					$scope.returnedCoins.push($scope.transactionCoins[i]);
				}
				aggregateReturnedCoins();
				$scope.transactionCoins = [];
				$scope.displayMessage = $scope.getDisplayMessage();
			}

		};

		//returns vending machine to original coin amounts
		//after an employee empties the profits
		$scope.resetBank = function(){
			$scope.bank = {
				nickel: 2,
				dime: 2,
				quarter: 0
			};
		};

		//returns vending machine to original inventory amounts
		//after an employee empties the profits
		$scope.restock = function(){
			$scope.inventory = {
				cola: 0,
				chips: 5,
				candy: 4
			};
		};

		$scope.clearCoin = function(coin){
			$scope.bank[coin] = 0;
		};

		$scope.takeChange = function(){
			$scope.returnedCoins = [];
			aggregateReturnedCoins();
		};

		// converts cents to dollars for display
		$scope.dollarFormat = function(cents){
			var formatted =  (cents / 100);
			formatted = '$' + formatted.toFixed(2);
			return formatted;
		};

		// for convenience in the wallet, naming the coins individually
		$scope.penny = {
			weight: 2.5,
			diameter: 19
		};
		$scope.nickel = {
			weight: 5,
			diameter: 21
		};
		$scope.dime = {
			weight: 2.26,
			diameter: 17
		};
		$scope.quarter = {
			weight: 5.6,
			diameter: 24
		};
		$scope.dollar = {
			weight: 8,
			diameter: 26
		};


  }]);








